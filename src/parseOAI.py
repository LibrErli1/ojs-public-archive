from lxml import etree
import requests

class parseOAI:

    def __init__(self, oaiUrl, ns):
        self.oaiUrl = oaiUrl
        self.ns = ns
        self.headers = {"User-Agent": "zhb luzern","Accept":"application/xml"} 

        
    def listRecords(self,resumptionToken=""):
        params = {"verb": "ListRecords", "metadataPrefix" : "oai_dc"}
        if resumptionToken != "":
            params["resumptionToken"] = resumptionToken
            params.pop("metadataPrefix")
        response = requests.get(self.oaiUrl,headers=self.headers,params=params)
        root = etree.fromstring(response.content)
        returnDict = {}
        returnDict["records"] = root.xpath(".//xmlns:ListRecords/xmlns:record/xmlns:header[not(@status='deleted')]/..",namespaces=self.ns)
        newResumptionToken = root.xpath(".//xmlns:resumptionToken",namespaces=self.ns)
        try:
            returnDict["resumptionToken"] = newResumptionToken[0].text
        except IndexError:
            returnDict["resumptionToken"] = None
        return returnDict
    
    def getRecord(self,recordId,mdFormat="marcxml"):
        params = {"verb": "GetRecord", "metadataPrefix" : mdFormat, "identifier" : recordId}
        response = requests.get(self.oaiUrl,headers=self.headers,params=params)
        root = etree.fromstring(response.content)
        return root

    def getMetadata(self, record, xpath):
        element = record.xpath(f"{xpath}",namespaces=self.ns)
        return element
    
    def parseMetadata(self, map, md, resultDet):
        if map["isMultiple"] == False:
            try:
                #print(f"{map['label']}: {md[0].text}")
                resultDet[map['label']] = md[0].text
            except IndexError:
                pass
        else:
            try:
                innerSet = []
                for metadata in md:
                    #print(f"{map['label']}: {metadata.text}")
                    innerSet.append(metadata.text)
                resultDet[map['label']] = innerSet
            except IndexError:
                pass
        return resultDet
    
    def parseMarcMetadata(self, map, md, resultDet):
        if map["mdFormat"] == "marcxml":
            try:
                innerSet = []
                for datafield in md:
                    innerDet = {}
                    for mapSubfield in map["subfields"]:
                        md = self.getMetadata(datafield,mapSubfield["xpath"])
                        self.parseMetadata(mapSubfield,md,innerDet)
                    innerSet.append(innerDet)
                #print(f"{map['label']}: {innerSet}")
                resultDet[map['label']] = innerSet
                return resultDet
            except KeyError:
                self.parseMetadata(map,md,resultDet)

    def runThroughRecords(self, records, resultSet, mapping):
        for record in records:
            resultDet = {}

            oaiId = self.getMetadata(record,".//xmlns:header/xmlns:identifier")
            resultDet["oaiId"] = oaiId[0].text
            marcRecord = self.getRecord(oaiId[0].text,"marcxml")
            
            #Metadata-Mapping and Serialization
            for map in mapping:
                    #check mdFormat and parse the correct Metadata
                    if map["mdFormat"] == "oai_dc":
                        md = self.getMetadata(record,map["xpath"])
                        self.parseMetadata(map,md,resultDet)
                    elif map["mdFormat"] == "marcxml":
                        md = self.getMetadata(marcRecord,map["xpath"])  
                        self.parseMarcMetadata(map,md,resultDet)

            print(resultDet)
            resultSet.append(resultDet)

            #break

        return resultSet
    
# Test the Class
if __name__ == '__main__':
    oaiUrl = "https://journals.ub.uni-heidelberg.de/index.php/ip/oai"
    oaiUrl = "https://www.globalbuddhism.org/oai"
    ns = {'xmlns' : 'http://www.openarchives.org/OAI/2.0/', 
      'oai_dc' : 'http://www.openarchives.org/OAI/2.0/oai_dc/', 
      'dc' : 'http://purl.org/dc/elements/1.1/',
      'slim': 'http://www.loc.gov/MARC21/slim'}
    
    oai = parseOAI(oaiUrl,ns)

    records = oai.listRecords()
    while records["resumptionToken"] is not None:
        print(records["records"])
        records = oai.listRecords(resumptionToken=records["resumptionToken"])
        print(records["resumptionToken"])
    print(f"last record charge:{records['records']}")