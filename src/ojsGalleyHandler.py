from bs4 import BeautifulSoup
import requests
import re
import os
if __name__ == '__main__':
    import wayback
else:
    from src import wayback

class ojsHTML:

    def __init__(self, ojsURL, letArchiveUrl=False):
        self.page=requests.get(ojsURL)
        self.ojsUrl = ojsURL
        self.ojsHtml=BeautifulSoup(self.page.content,"html.parser")
        self.letArchiveUrl = letArchiveUrl

    #Method to find the URL to an given Format
    def getGalleyUrl(self,format="HTML"):
        res = self.ojsHtml.find('a', attrs={'class': 'obj_galley_link'}, string=lambda text: format in text)
        returnDict = {}
        try:
            response = requests.get(res["href"])

            returnDict["status_code"] = response.status_code
            returnDict["content_type"] = response.headers.get('Content-Type')

            if format=="EPUB" and response.headers.get("Content-Type") == "application/epub+zip":
                returnDict["galleyUrl"] = res["href"]
                returnDict["fileEnding"] = "epub"
            elif format == "PDF":
                if response.headers.get("Content-Type") == "application/pdf":
                    returnDict["galleyUrl"] = res["href"]
                else:
                    #pdf Galley-Link resolves to OJS-Viewer, parse the real pdf-URL from <a class='download'> Tag here
                    pdfHtml = BeautifulSoup(response.content,"html.parser")
                    resPdf = pdfHtml.find("a", {"class": "download"})
                    returnDict["galleyUrl"] = resPdf["href"]
                    returnDict["content_type"] = response.headers.get('Content-Type')
                returnDict["fileEnding"] = "pdf"
            elif format == "HTML":
                #Check galley_view_without_iframe or not
                returnDict["fileEnding"] = "html"
                htmlGalley = BeautifulSoup(response.content,"html.parser")
                htmlContainer = htmlGalley.find("div", attrs={"id":"htmlContainer", "class":"galley_view_without_iframe"})
                if htmlContainer is not None:
                    returnDict["galleyUrl"] = res["href"]
                else:
                    htmlContainer = htmlGalley.find("div", attrs={"id":"htmlContainer", "class":"galley_view"})
                    #HTML Galley with iframe - fetch HTML article from iframe.src
                    if htmlContainer is not None:
                        iFrame = htmlGalley.find("iframe",{"name":"htmlFrame"})
                        returnDict["galleyUrl"]  = (iFrame["src"].strip())

            # Save HTML Page in Wayback Machine
            if self.letArchiveUrl == True:
                returnDict["archiveUrl"] = wayback.getWaybackUrl(returnDict["galleyUrl"])
            
        except TypeError:
            returnDict["message"] = f"No {format}-Galley found"
            returnDict["format"] = format

        return returnDict 

    #Method to download a galley file
    def downloadFile(self, fileUrl, fileName, filePath=""):
        response = requests.get(fileUrl)
        returnDict = {}
        returnDict["status_code"] = response.status_code
        returnDict["content_type"] = response.headers.get('Content-Type')

        #Check local Download Directory
        if filePath == "":
            filePath = "files"
        if not os.path.exists(filePath):
            os.makedirs(filePath)
        #Download File to local Path
        if response.status_code == 200:
            with open(f"{filePath}/{fileName}", 'wb') as f:
                f.write(response.content)
                returnDict["fileName"] = fileName
        return returnDict

    #Method to extract "filename" from the Galley-URL, you could provide of course any other generic filename in the download method
    def getFileName(self, fileUrl=""):
        regex = r"[^\/]+$"
        if fileUrl == "":
            fileUrl = self.ojsUrl
        matches = re.finditer(regex, fileUrl, re.MULTILINE)
        for matchNum, match in enumerate(matches, start=1):
            return match.group(0)

# Test the Class
if __name__ == '__main__':
    #ojsHTML = ojsHTML("https://journals.ub.uni-heidelberg.de/index.php/ip/article/view/89240")
    ojsHTML = ojsHTML("https://www.globalbuddhism.org/article/view/3409", letArchiveUrl=True)
    localFilePath = "files/test"
    fileName = ojsHTML.getFileName()

    galleyFormats = ["HTML", "EPUB", "PDF"]
    for galleyFormat in galleyFormats:
        galleyUrl = ojsHTML.getGalleyUrl(format = galleyFormat)
        print(galleyUrl)
        try:
                ojsHTML.downloadFile(fileUrl=galleyUrl["galleyUrl"],fileName=f"{fileName}.{galleyUrl['fileEnding']}",filePath=localFilePath)
        except KeyError:
                pass
