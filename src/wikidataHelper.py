from SPARQLWrapper import SPARQLWrapper, JSON

def CheckArticlebyPID(property,pidValue):
    #print(IDN)
    sparql = SPARQLWrapper("https://query.wikidata.org/sparql",agent='Checking articles by PID')
    query = f"SELECT * WHERE {{ ?article wdt:{property} \"{pidValue}\". }}"
    sparql.setQuery(query)
    sparql.setReturnFormat(JSON)
    results = sparql.query().convert()

    #print (results)
    for result in results["results"]["bindings"]:
        return result["article"]["value"]

#Test the Functions
if __name__ == '__main__':
    print(CheckArticlebyPID('P356','10.11588/IP.2015.1.16488')) 