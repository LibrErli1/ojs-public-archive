import waybackpy

def getWaybackUrl(url):
    user_agent = "mailto:zentralgut@zhbluzern.ch"
    wayback = waybackpy.Url(url, user_agent)
    try:
        archive = wayback.newest()
        return archive.archive_url
    except:
        try:
            archive = wayback.save()
            return archive.archive_url
        except:
            return None

#Test the Functions
if __name__ == '__main__':
    url = "https://www.globalbuddhism.org/article/download/3409/3602?inline=1"
    print(getWaybackUrl(url)) 