from src import parseOAI
from src import wikidataHelper as WD
from src import wayback
from src import ojsGalleyHandler as ojs


import json

# ============================================
# Configure your variables

oaiUrl = "https://journals.ub.uni-heidelberg.de/index.php/ip/oai"
#oaiUrl = "https://www.globalbuddhism.org/oai"

localFilePath = "files/ip"

galleyFormats = ["PDF", "HTML", "EPUB"] # if a format is not given on the page, it won't be downloaded and ignored.

jsonOutputFile = "Output_infoprax.json"

letDownloadFiles = True
letArchiveUrl = True

ns = {'xmlns' : 'http://www.openarchives.org/OAI/2.0/', 
      'oai_dc' : 'http://www.openarchives.org/OAI/2.0/oai_dc/', 
      'dc' : 'http://purl.org/dc/elements/1.1/',
      'slim': 'http://www.loc.gov/MARC21/slim'}

mapping = [
    {'xpath': ".//*[local-name()='setSpec']", "mdFormat" : "oai_dc", 'label': 'set', 'isMultiple': True},
    {'xpath': ".//dc:title[@xml:lang='de-DE']", "mdFormat" : "oai_dc", 'label': 'title_DE', 'isMultiple': False},
    {'xpath': ".//dc:title[@xml:lang='en-US']", "mdFormat" : "oai_dc", 'label': 'title_EN', 'isMultiple': False},
    {'xpath': ".//slim:datafield[@tag='100' or @tag='720']", "mdFormat" : "marcxml", 'label': 'creator', 'isMultiple': True, "subfields" : [ { "xpath": ".//slim:subfield[@code='a']", "label": "creator", 'isMultiple': False}, { "xpath": ".//slim:subfield[@code='0'][contains(.,'orcid.org')]", "label" : "creator_orcid", 'isMultiple': False}]},
    {'xpath': ".//dc:subject[@xml:lang='de-DE']", "mdFormat" : "oai_dc", 'label': 'subject', 'isMultiple': True},
    {'xpath': './/dc:date', "mdFormat" : "oai_dc", 'label': 'date', 'isMultiple': False},
    {'xpath': ".//dc:identifier[starts-with(.,'urn')]", "mdFormat" : "oai_dc", 'label': 'urn', 'isMultiple': False},
    {'xpath': ".//dc:identifier[starts-with(.,'http')]", "mdFormat" : "oai_dc", 'label': 'fulltextUrl', 'isMultiple': False},
    {'xpath': ".//dc:identifier[starts-with(.,'10.')]", "mdFormat" : "oai_dc", 'label': 'doi', 'isMultiple': False},
    {'xpath': ".//dc:source", "mdFormat" : "oai_dc", 'label': 'volume', 'isMultiple': False}
]

# End of Configuration
# ============================================

oai = parseOAI.parseOAI(oaiUrl,ns)
records = oai.listRecords()
#print(records)
resultSet = []

# ======================
# OAI-Parser - Get Metadata
# Parse the OAI-Result of the Journal
while records["resumptionToken"] is not None:
      # do stuff with records
      oai.runThroughRecords(records["records"],resultSet,mapping)
      records = oai.listRecords(resumptionToken=records["resumptionToken"])
# do last stuff with records (or even if there were no resumptionToken at all)
oai.runThroughRecords(records["records"],resultSet,mapping)


# ======================
# Afterwards the resultSet Object is the main operating data object

#Check parsed Articles whether they are indexed in Wikidata or not
for record in resultSet:
      try:
            record["articleWD"] = WD.CheckArticlebyPID("P356",record["doi"].upper())
      except KeyError:
            print(f"no DOI given: {record}")


#Download Galley Files
if letDownloadFiles == True:
      for record in resultSet:
            #parse OJS HTML Landing Page
            ojsHTML = ojs.ojsHTML(record["fulltextUrl"], letArchiveUrl)
            if letArchiveUrl == True:
                  record["archiveUrl"] = wayback.getWaybackUrl(record["fulltextUrl"])
            fileName = ojsHTML.getFileName()

            #download articles in given galley formats (configure section list galleyFormats = ["PDF", "HTML", "EPUB"])
            for galleyFormat in galleyFormats:
                  galleyUrl = ojsHTML.getGalleyUrl(format = galleyFormat)
                  print(galleyUrl)
                  try:
                        ojsHTML.downloadFile(fileUrl=galleyUrl["galleyUrl"],fileName=f"{fileName}.{galleyUrl['fileEnding']}",filePath=localFilePath)
                        record[f"{galleyUrl['fileEnding']}File"] = f"{fileName}.{galleyUrl['fileEnding']}"
                        record[f"{galleyUrl['fileEnding']}ArchiveUrl"] = galleyUrl['archiveUrl']
                  except KeyError:
                        pass


# Write metadata (resultSet) into *.json
json_str = json.dumps(resultSet)
# And then write that string to a file
with open(f"{localFilePath}/{jsonOutputFile}", 'w') as f:
    f.write(json_str)