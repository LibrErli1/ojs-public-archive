# Toolbox to public archive Open Access Journals

> This script is a totally refactorized and enhanced version of the Jupyter Notebook [Parse OJS OAI 2 Wikidata](https://gitlab.com/LibrErli1/parse_ojs_oai_2_wikidata) (presented at [WikiCite 2020 conference](https://meta.wikimedia.org/wiki/WikiCite/2020_Virtual_conference/Align_your_Open_Access_Journal_with_Wikidata_-_using_Python_and_OpenRefine))

This script does the following steps for OJS hosted Journals:
* parse metadata from OAI-PMH (e.g. for journals hosted in OJS) and serialize them into a *.json file
* parse the OJS article landing page and download the galleys for those formats configured here

With the fetched and downloaded (meta)data this script allows you to do the following steps:
* Upload Metadata to Wikidata with OpenRefine by import `Output.json`
* Save your public avaiable URLs in Internet Archives Wayback Machine
* Upload your files to Zenodo
* Update the Wikidata-Items with Wayback Archive-URLs and Zenodo-IDs.

## Configuration

In `run.py` you have to configure following parameters: 

| Variable      | Description                              | Example       |
|---------------|------------------------------------------|---------------|
| `oaiUrl` | Give an URL to an OAI-PMH Endpoint without further URL Paramenter | `"https://www.globalbuddhism.org/oai"` |
| `localFilePath` | Provide a local file path on your device where the article files (galley) are stored | `"files/journalAbbreviation"` |
| `galleyFormats` | Provide a list of Formats of Galleys as given on the OJS landing page | `["PDF", "HTML", "EPUB"]` |
| `jsonOutputFile` | Provide a filename for the metadata output json file | `Output_JournalXY.json` |
| `ns` | Provide the namespaces given in your OAI-PMH respond | ```ns = {'xmlns' : 'http://www.openarchives.org/OAI/2.0/', 'oai_dc' : 'http://www.openarchives.org/OAI/2.0/oai_dc/',  'dc' : 'http://purl.org/dc/elements/1.1/', 'slim': 'http://www.loc.gov/MARC21/slim'}``` |
| `mapping` | Provide a list of dictionaries containing the mapping of metadata. The default value will provide you certain basic information |  ```mapping = [  {'xpath': ".//*[local-name()='setSpec']", "mdFormat" : "oai_dc", 'label': 'set', 'isMultiple': True},    {'xpath': ".//dc:title[@xml:lang='de-DE']", "mdFormat" : "oai_dc", 'label': 'title_DE', 'isMultiple': False},    {'xpath': ".//dc:title[@xml:lang='en-US']", "mdFormat" : "oai_dc", 'label': 'title_EN', 'isMultiple': False},    {'xpath': ".//slim:datafield[@tag='100' or @tag='720']", "mdFormat" : "marcxml", 'label': 'creator', 'isMultiple': True, "subfields" : [ { "xpath": ".//slim:subfield[@code='a']", "label": "creator", 'isMultiple': False}, { "xpath": ".//slim:subfield[@code='0'][contains(.,'orcid.org')]", "label" : "creator_orcid", 'isMultiple': False}]},    {'xpath': ".//dc:subject[@xml:lang='de-DE']", "mdFormat" : "oai_dc", 'label': 'subject', 'isMultiple': True},    {'xpath': './/dc:date', "mdFormat" : "oai_dc", 'label': 'date', 'isMultiple': False},    {'xpath': ".//dc:identifier[starts-with(.,'urn')]", "mdFormat" : "oai_dc", 'label': 'urn', 'isMultiple': False},    {'xpath': ".//dc:identifier[starts-with(.,'http')]", "mdFormat" : "oai_dc", 'label': 'fulltextUrl', 'isMultiple': False},    {'xpath': ".//dc:identifier[starts-with(.,'10.')]", "mdFormat" : "oai_dc", 'label': 'doi', 'isMultiple': False},   {'xpath': ".//dc:source", "mdFormat" : "oai_dc", 'label': 'volume', 'isMultiple': False}]```|
| `letDownloadFiles` | Boolean - `True`: Files from OJS will be scraped and downloaded to `localFilePath` | `letDownloadFiles = True`  |
| `letArchiveUrl` | Boolean - `True`: URLs from OJS will be saved in the Wayback Machine (Default is `False`, archiving is very time consuming) | `letArchiveUrl = True`     |

## Run the File

After set your individual configurations run the file with `python3 run.py`