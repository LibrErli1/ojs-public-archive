# ToDo

## General
- [ ] Allow different parts of the script tools for Output.json (e.g. go through a certain `output.json` and fetch all newest archives url or look for WD-Items)

## OAI-Parser `ojsGalleyHandler.py`

## WayBack-Machine - fullwork-URL

## HTML-Parser - information from article landing page not given in OAI metadata at all
- [ ] Extract Images from Article and save them local Search for `<img>` as child of `<div#htmlContainer>` (when directly in OJS layout) or in body for `<iframe>` when html galley is displayed via `<iframe>`
- [ ] Parse Abstract from Website (useful for Zenodo-Ingest)

## Zenodo-Upload
- [ ] Zenodo-Upload (make use of `ZenodoRest.py` from https://github.com/zhbluzern/Zenodo-pyCLI-Uploader)

## Wikimedia Commons Upload
- [ ] Extracted Images could be uploaded to Wikimedia Commons

# Done

## OAI-Parser `ojsGalleyHandler.py`
- [x] Deal with deleted records: e.g. view-source:https://www.globalbuddhism.org/oai?verb=ListRecords&metadataPrefix=oai_dc
- [x] Deal with resumpton token: https://www.globalbuddhism.org/oai?verb=ListRecords&metadataPrefix=oai_dc

## Galley-Parser
- [x] Fetch PDF
  - [x] check whether Galley URL is a PDF or an HTML page, on HTML page look for `<a class="download">`
  - [x] implement pdf deep fetching in the more genere method getGalleyUrl()
- [x] Fetch HTML
  - [x] e.g. JGB displays the HTML article galley as IFRAME. (e.g. https://www.globalbuddhism.org/article/view/3409) -> IP has class div#htmlContainer.galley_view_without_iframe // JGB has div#htmlContainer.galley_view
- [x] Fetch EPUB and other formats

## WayBack-Machine - fullwork-URL
- [x] Save Article Landing Page (done in `run.py`)
- [x] Save Galleys (done in `ojsGalleyHandler.py`)

## General
- [x] Add config parameter `letDownloadFiles : True|False` to configure whether file download should be done or not.
- [x] Add config parameter `letArchiveUrl` to configure wheter an URL should be archived in the WaybackMachine or not. 
- [x] Save Output Json Files in the Output-Directory `localFilePath` as configured